.. blockdiag on readthedocs documentation master file, created by
   sphinx-quickstart on Wed May 16 19:04:53 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to blockdiag on readthedocs's documentation!
====================================================
Sample
------

.. blockdiag::

    blockdiag admin {
      top_page -> config -> config_edit -> config_confirm -> top_page;
    }

Usage
-----

#. Add :mod:`sphinxcontrib.blockdiag` extention to :file:`conf.py`::

       extensions = ['sphinxcontrib.blockdiag', ]

#. Add :file:`requirements.txt` to your document's directory (e.g. ``docs``, ``doc``)::

       sphinxcontrib-blockdiag==1.1.1

#. Setup `readthedocs`_ project

   * **Use virtualenv**: cheked
   * **Requirements file**: your :file:`requirements.txt`

.. image:: _static/setup.png
   :alt: screen shots
   :align: center

.. toctree::
   :maxdepth: 2

.. _readthedocs: http://readthedocs.org/

Tips
----
Use unicode chars
^^^^^^^^^^^^^^^^^

.. blockdiag::

    blockdiag admin {
       // Set M17N text using label property.
       A [label = "起"];
       B [label = "承"];
       C [label = "転"];
       D [label = "結"];

       A -> B -> C -> D;

       // Use M17N text directly (need to quote).
       春 -> 夏 -> 秋 -> 冬;

       // Use M17N text including symbol characters (need to quote).
       "春は 曙" -> "夏 = 夜" -> "秋.夕暮れ" -> "冬 & つとめて";
    }

#. Downloading & storing unicode font (:file:`*.ttf`) to ``_static`` directory.
#. Specify it in :file:`conf.py`::

       # Use IPA P Gothic fonts: http://ossipedia.ipa.go.jp/ipafont/
       blockdiag_fontpath = '_static/ipagp00303/ipagp.ttf'

Link
----

* Hosted at: http://blockdiag-readthedocs.rtfd.org/
* Source: https://bitbucket.org/Surgo/blockdiag-readthedocs/

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
